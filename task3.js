// Write a function that takes two arrays (a and b) as arguments
// Create an object that has properties with keys 'a' and corresponding values 'b'
// Return the object

function myFunction(a, b) {
  // Your code here...

  //solution 1
  // const result = Object.fromEntries(a.map((key, index) => [key, b[index]]));
  // console.log(result);

  //solution 2
  // const result = {};
  // a.forEach((key, i) => (result[key] = b[i]));
  // console.log(result);
  // return result

  // solution 3
  const result = Object.assign.apply(
    {},
    a.map((v, i) => ({ [v]: b[i] }))
  );
  console.log(result); //returns {"foo": 11, "bar": 22, "baz": 33}
}

myFunction(["a", "b", "c"], [1, 2, 3]);
// Expected {a:1,b:2,c:3}
myFunction(["w", "x", "y", "z"], [10, 9, 5, 2]);
// Expected {w:10,x:9,y:5,z:2}
myFunction([1, "b"], ["a", 2]);
// Expected {1:'a',b:2}
