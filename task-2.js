// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...
function productsFactory(name, info, price, chooseBtn) {
  return {
    name: name,
    infos: `information: ${info}`,
    price: price + " sum",
    chooseBtn: chooseBtn,
  };
}
const product1 = productsFactory("Max Box 'Popular'", "meat lawash", 35000);
console.log(product1);

// Constructor Function
// You code here ...

function ProductsConstructor(name, info, price, chooseBtn) {
  (this.name = name),
    (this.infos = `information: ${info}`),
    (this.price = price + " sum"),
    (this.chooseBtn = chooseBtn);
}

const product2 = new ProductsConstructor(
  "Max Box 'Retro'",
  "meat shawarma",
  30000
);
console.log(product2);
